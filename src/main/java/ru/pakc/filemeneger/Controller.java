package ru.pakc.filemeneger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    TableView filesTable;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void btnExitAction(ActionEvent actionEvent) {   // метод который реализует нажатие кнопки
        Platform.exit();    // Выполняет выход из программы
    }


}