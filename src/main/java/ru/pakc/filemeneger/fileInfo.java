package ru.pakc.filemeneger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class fileInfo {
    public enum FileType {
        File("F"), DERECTORY("D");
        private String name;

        public String getName() {
            return name;
        }

        FileType(String name) {
            this.name = name;

        }

        private String fileName;
        private FileType type;
        private long size;
        private LocalDateTime lastModified;

        public void setName(String name) {
            this.name = name;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public FileType getType() {
            return type;
        }

        public void setType(FileType type) {
            this.type = type;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        public LocalDateTime getLastModified() {
            return lastModified;
        }

        public void setLastModified(LocalDateTime lastModified) {
            this.lastModified = lastModified;
        }

        public void FileInfo(Path path) {
            try {
                this.fileName = path.getFileName().toString();
                this.size = Files.size(path);
                this.type = Files.isDirectory(path) ? FileType.DERECTORY : FileType.File;
            } catch (IOException e) {
                throw new RuntimeException("Enable to create file info from path");
            }
        }
    }
}
