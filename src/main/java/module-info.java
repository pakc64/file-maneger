module ru.pakc.filemeneger {
    requires javafx.controls;
    requires javafx.fxml;


    opens ru.pakc.filemeneger to javafx.fxml;
    exports ru.pakc.filemeneger;
}